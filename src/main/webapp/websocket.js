var ws;

function connect() {
    var username = document.getElementById("username").value;

    var host = document.location.host;
    var pathname = document.location.pathname;

    ws = new WebSocket("ws://localhost:8887/ws/chat/" + username);

    ws.onmessage = function(event) {
        var log = document.getElementById("log");
        console.log(event.data);
        var message = JSON.parse(event.data);
        log.innerHTML += message.sender + " : " + message.message + "\n";
    };
}

function send() {
    var content = document.getElementById("msg").value;
    console.log(content)
    var json = JSON.stringify({
        "message":content
    });

    ws.send(json);
}